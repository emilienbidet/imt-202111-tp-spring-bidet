package fr.imt.student_manager.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Entity
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String title;
    private String description;

    @OneToMany(mappedBy = "course")
    private List<Registration> registrations = new ArrayList();

    public Course() {
    }

    public Course (String title, String description) {
        this.title = title;
        this.description = description;
    }

    /**
     * Get course id
     * @return course id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set course id
     * @param id course id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get course title
     * @return course title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set course title
     * @param title course title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Get course description
     * @return course description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set course description
     * @param description course description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get course registrations
     * @return course registrations
     */
    public List<Registration> getRegistrations() {
        return registrations;
    }

    /**
     * Set course registrations
     * @param registrations course registrations
     */
    public void setRegistrations(List<Registration> registrations) {
        this.registrations = registrations;
    }
}
