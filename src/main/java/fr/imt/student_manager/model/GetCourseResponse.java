package fr.imt.student_manager.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.List;


@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class GetCourseResponse {
    private int id;
    private String title;
    private String description;
    private List<GetCourseResponseStudent> students;

    public GetCourseResponse() {
    }

    public GetCourseResponse(int id, String title, String description, List<GetCourseResponseStudent> students) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.students = students;
    }
}
