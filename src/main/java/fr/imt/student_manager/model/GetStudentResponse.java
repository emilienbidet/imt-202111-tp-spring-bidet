package fr.imt.student_manager.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.List;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class GetStudentResponse {
    private Integer id;
    private String firstName;
    private String lastName;
    private List<GetStudentResponseCourse> courses;

    public GetStudentResponse() {
    }

    public GetStudentResponse(Integer id, String firstName, String lastName, List<GetStudentResponseCourse> courses) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.courses = courses;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
