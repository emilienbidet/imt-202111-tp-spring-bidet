package fr.imt.student_manager.service;

import fr.imt.student_manager.model.Registration;
import fr.imt.student_manager.repository.RegistrationRepository;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {

    private RegistrationRepository registrationRepository;

    public RegistrationService(RegistrationRepository registrationRepository) {
        this.registrationRepository = registrationRepository;
    }

    public Iterable<Registration> findAll() {
        return registrationRepository.findAll();
    }

    public Registration save(Registration registration) {
        return registrationRepository.save(registration);
    }
}
