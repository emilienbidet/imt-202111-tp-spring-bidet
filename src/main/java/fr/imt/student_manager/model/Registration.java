package fr.imt.student_manager.model;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
public class Registration {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;

    @Min(0)
    @Max(20)
    @Digits(integer = 2, fraction = 2)
    Double grade;

    public Registration() {
    }
    public Registration(Course savedCourse, Student student, double grade) {
        this.course = savedCourse;
        this.student = student;
        this.grade = grade;
    }

    /**
     * Get the registration course
     * @return course
     */
    public Course getCourse() {
        return course;
    }

    /**
     * Set the registration course
     * @param course Course
     */
    public void setCourse(Course course) {
        this.course = course;
    }

    /**
     * Get the registration student
     * @return student
     */
    public Student getStudent() {
        return student;
    }

    /**
     * Set the registration student
     * @param student
     */
    public void setStudent(Student student) {
        this.student = student;
    }

    /**
     * Get the registration grade
     * @return grade
     */
    public double getGrade() {
        return grade;
    }

    /**
     * Set the registration grade
     * @param grade
     */
    public void setGrade(double grade) {
        this.grade = grade;
    }
}