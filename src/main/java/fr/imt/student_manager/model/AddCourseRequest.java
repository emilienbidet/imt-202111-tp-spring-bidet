package fr.imt.student_manager.model;

import java.util.List;

public class AddCourseRequest {
    private String title;
    private String description;
    private List<AddCourseRequestStudent> students;

    public String getTitle() {
        return title;
    }
    public String getDescription() {
        return description;
    }
    public List<AddCourseRequestStudent> getStudents() {
        return students;
    }
}
