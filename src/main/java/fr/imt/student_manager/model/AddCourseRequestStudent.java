package fr.imt.student_manager.model;

public class AddCourseRequestStudent {
    private String firstName;
    private String lastName;
    private Double grade;

    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public Double getGrade() {
        return grade;
    }
}