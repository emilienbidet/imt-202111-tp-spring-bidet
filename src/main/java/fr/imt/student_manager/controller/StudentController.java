package fr.imt.student_manager.controller;

import fr.imt.student_manager.model.AddStudentRequest;
import fr.imt.student_manager.model.GetStudentResponse;
import fr.imt.student_manager.model.Student;
import fr.imt.student_manager.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping(path="/student")
public class StudentController {

    private StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    /**
     * Get all students
     * @return The list of students
     */
    @GetMapping(path="/")
    public @ResponseBody Iterable<GetStudentResponse> getAllStudents(
            @RequestParam(required = false) String firstName,
            @RequestParam(required = false) String lastName){
        return studentService.findAll(firstName, lastName);
    }

    /**
     * Add a student
     * @param student
     * @return The student added
     */
    @PostMapping(path="/")
    public @ResponseBody GetStudentResponse addStudent(@RequestBody AddStudentRequest student){
        return studentService.save(student);
    }


    /**
     * Get a student by id
     * @param id
     * @return The student
     */
    @GetMapping(path="/{id}")
    public @ResponseBody Optional<GetStudentResponse> getStudent(@PathVariable Integer id){
        return studentService.findById(id);
    }
}
