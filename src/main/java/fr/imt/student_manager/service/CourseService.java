package fr.imt.student_manager.service;

import fr.imt.student_manager.model.*;
import fr.imt.student_manager.repository.CourseRepository;
import fr.imt.student_manager.repository.RegistrationRepository;
import fr.imt.student_manager.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;


@Service
public class CourseService {

    private CourseRepository courseRepository;
    private StudentRepository studentRepository;
    private RegistrationRepository registrationRepository;

    @Autowired
    public CourseService(CourseRepository courseRepository,
                         StudentRepository studentRepository,
                         RegistrationRepository registrationRepository) {
        this.courseRepository = courseRepository;
        this.studentRepository = studentRepository;
        this.registrationRepository = registrationRepository;
    }

    public GetCourseResponse formatCourse(Course course, Double gradeLte) {
        List<GetCourseResponseStudent> students = new ArrayList<>();
        course.getRegistrations().forEach(registration -> {
            Student student = registration.getStudent();
            students.add(new GetCourseResponseStudent(student.getId(), student.getFirstName(), student.getLastName(), registration.getGrade()));
        });
        if (gradeLte != null) {
            students.removeIf(student -> student.getGrade() > gradeLte);
        }
        return new GetCourseResponse(course.getId(), course.getTitle(), course.getDescription(), students);
    }

    /**
     * Get all courses
     * @return List of courses
     */
    public Iterable<GetCourseResponse> findAll(Double gradeLte) {
        return this.courseRepository.findAll().stream().map(course -> formatCourse(course, gradeLte)).collect(java.util.stream.Collectors.toList());
    }

    /**
     * Get course by id
     * @param id Course id
     * @return Course
     */
    public Optional<GetCourseResponse> findById(Integer id, Double gradeLte) {
        Optional<Course> course = this.courseRepository.findById(id);
        if (course.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(this.formatCourse(course.get(), gradeLte));
    }

    /**
     * Save course
     * @param courseRequest Course
     * @return Course created
     */
    @Transactional
    public GetCourseResponse save(AddCourseRequest courseRequest) {
        // save the course
        Course savedCourse = this.courseRepository.save(new Course(courseRequest.getTitle(), courseRequest.getDescription()));

        // save students
        List<Student> students = new ArrayList<>();
        List<Registration> registrations = new ArrayList<>();
        for (AddCourseRequestStudent addCourseRequestStudent : courseRequest.getStudents()) {
            Student savedStudent = this.studentRepository.save(new Student(addCourseRequestStudent.getFirstName(), addCourseRequestStudent.getLastName()));
            students.add(savedStudent);

            registrations.add(new Registration(savedCourse, savedStudent, addCourseRequestStudent.getGrade()));
        }

        // save registrations
        this.registrationRepository.saveAll(registrations);
        savedCourse.setRegistrations(registrations);

        return formatCourse(savedCourse, null);
    }

}
