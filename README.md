<div id="top"></div>


<!-- PROJECT LOGO -->
<br />
<div align="center">
<h3 align="center">Student manager</h3>
</div>

### Built With

* [Spring Boot](https://spring.io/projects/spring-boot)
* [Spring Boot initializer](https://start.spring.io/)
* [Spring Boot JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Spring Boot Hibernate](https://www.baeldung.com/spring-boot-hibernate)
* [Spring Boot MySQL](https://spring.io/guides/gs/accessing-data-mysql/)
* [Docker](https://docker.com)

## Getting Started

To start the project, simply run the following command:

### Prerequisites

- Docker
- Java 8

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/emilienbidet/imt-202111-tp-spring-bidet
   ```
2. Up the MySQL Docker image
   ```sh
   cd imt-202111-tp-spring-bidet
   docker-compose up
   ```
   
3. Run the application
   ```sh
   mvn spring-boot:run
   ```

## Usage

Database administration is under the following URL:
[http://localhost:8081/](http://localhost:8081/)

API base url is under the following URL:
[http://localhost:8080/](http://localhost:8080/)

Use postman collections to test the API: ```/docs/postman/```

## Roadmap

- [ ] Make the course post accept student id
- [ ] Add units tests
- [ ] Add integration tests

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## Contact

Emilien Bidet - [@bidet_emilien](https://twitter.com/bidet_emilien) - emilienbdt@gmail.com
