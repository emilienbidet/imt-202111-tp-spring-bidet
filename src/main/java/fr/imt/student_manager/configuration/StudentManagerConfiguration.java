package fr.imt.student_manager.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "fr.imt.student_manager")
@EntityScan("fr.imt.student_manager")
public class StudentManagerConfiguration {
}
