package fr.imt.student_manager.controller;

import fr.imt.student_manager.model.AddCourseRequest;
import fr.imt.student_manager.model.GetCourseResponse;
import fr.imt.student_manager.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@Controller
@RequestMapping("/course")

public class CourseController {

    private CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }


    @GetMapping(path = "/")
    public @ResponseBody
    Iterable<GetCourseResponse> getAllCourses(@RequestParam(value = "grade_lte", required = false) Double gradeLte){
        return this.courseService.findAll(gradeLte);
    }

    @PostMapping(path = "/")
    public @ResponseBody
    GetCourseResponse addNewCourse(@RequestBody AddCourseRequest courseRequest) {
        return this.courseService.save(courseRequest);
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody
    Optional<GetCourseResponse> getCourseById(@PathVariable Integer id, @RequestParam(value = "grade_lte", required = false) Double gradeLte){
        return this.courseService.findById(id, gradeLte);
    }

}
