package fr.imt.student_manager.model;

public class AddStudentRequest {
    private String lastName;
    private String firstName;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
