package fr.imt.student_manager.model;

import java.util.Set;

public interface StudentInfo {
    Integer getId();

    String getFirstName();

    String getLastName();

    Set<CourseInfo> getCourses();

    interface CourseInfo {
        Integer getId();

        String getTitle();

        String getDescription();

        double getGrade();
    }
}
