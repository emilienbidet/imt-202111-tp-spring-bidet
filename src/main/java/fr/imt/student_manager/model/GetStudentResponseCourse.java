package fr.imt.student_manager.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class GetStudentResponseCourse {
    private int id;
    private String title;
    private String description;

    public GetStudentResponseCourse() {

    }

    public GetStudentResponseCourse(int id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }
}
