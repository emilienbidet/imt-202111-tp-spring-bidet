package fr.imt.student_manager.controller;

import fr.imt.student_manager.model.Registration;
import fr.imt.student_manager.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/registration")
public class RegistrationController {
    private RegistrationService registrationService;

    @Autowired
    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @GetMapping("/")
    public @ResponseBody Iterable<Registration> findAll() {
        return registrationService.findAll();
    }

    @PostMapping("/")
    public @ResponseBody Registration save(Registration registration) {
        return registrationService.save(registration);
    }
}