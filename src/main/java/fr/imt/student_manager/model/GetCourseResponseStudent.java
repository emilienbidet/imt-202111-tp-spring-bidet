package fr.imt.student_manager.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class GetCourseResponseStudent {
    private Integer id;
    private String firstName;
    private String lastName;
    private Double grade;

    public GetCourseResponseStudent() {
    }

    public GetCourseResponseStudent(Integer id, String firstName, String lastName, Double grade) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.grade = grade;
    }

    public Double getGrade() {
        return grade;
    }
}
