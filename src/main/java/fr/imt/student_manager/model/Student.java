package fr.imt.student_manager.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String firstName;
    private String lastName;

    @OneToMany(mappedBy = "student")
    private List<Registration> registrations = new ArrayList();

    public Student() {
    }

    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public List<Registration> getRegistrations() {
        return registrations;
    }

    public void setRegistrations(List<Registration> registrations) {
        this.registrations = registrations;
    }

    /**
     * Get the student's id
     * @return the student's id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Get the student's first name
     * @return first name of the student
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the student's first name
     * @param firstName the student's first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Get the student's last name
     * @return the student's last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the student's last name
     * @param lastName the student's last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
