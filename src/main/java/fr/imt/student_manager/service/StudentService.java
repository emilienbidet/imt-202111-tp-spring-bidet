package fr.imt.student_manager.service;

import fr.imt.student_manager.model.*;
import fr.imt.student_manager.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    private StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }


    public GetStudentResponse formatStudent(Student student) {
        List<GetStudentResponseCourse> courses = new ArrayList<>();
        student.getRegistrations().forEach(registration -> {
            Course course = registration.getCourse();
            courses.add(new GetStudentResponseCourse(course.getId(), course.getTitle(), course.getDescription()));
        });
        return new GetStudentResponse(student.getId(), student.getFirstName(), student.getLastName(), courses);

    }



    /**
     * Get all students
     * @return The list of students
     */
    public Iterable<GetStudentResponse> findAll(String firstName, String lastName) {
        List<GetStudentResponse> students = studentRepository.findAll().stream().map(this::formatStudent).collect(java.util.stream.Collectors.toList());
        if (firstName != null) {
            students = students.stream().filter(student -> student.getFirstName().equals(firstName)).collect(java.util.stream.Collectors.toList());
        }
        if (lastName != null) {
            students = students.stream().filter(student -> student.getLastName().equals(lastName)).collect(java.util.stream.Collectors.toList());
        }
        return students;
    }

    /**
     * Get a student by its id
     * @param id The id of the student
     * @return The student
     */
    public Optional<GetStudentResponse> findById(Integer id) {
        Optional<Student> student = this.studentRepository.findById(id);

        if(student.isEmpty())
            return Optional.empty();

        return Optional.of(this.formatStudent(student.get()));
    }


    /**
     * Create a new student
     * @param student The student to create
     * @return The created student
     */
    @Transactional
    public GetStudentResponse save(AddStudentRequest student) {
        return this.formatStudent(studentRepository.save(new Student(student.getFirstName(), student.getLastName())));
    }
}
